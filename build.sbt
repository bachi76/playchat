name := "play-chat"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
  //"ws.wamplay" %% "wamplay" % "0.1.6"
)     

play.Project.playJavaSettings

//resolvers ++= Seq("WAMPlay Repository" at "http://blopker.github.com/maven-repo/")