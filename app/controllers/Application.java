package controllers;

import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class Application extends Controller {
  
    public static Result index() {
    	
		return ok("Server is ready at " + Http.Context.current().request().host());    
    }
  
}
