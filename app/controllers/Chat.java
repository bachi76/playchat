package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.WebSocket;

import java.util.ArrayList;
import java.util.List;

public class Chat extends Controller {

	public static Result index() {
		List<Chatroom> roomList = new ArrayList<Chatroom>(Chatroom.activeRooms.values());
		return ok(views.html.chat.home.render(roomList));
	}

	public static Result createRoom(String name, String makePublic) {

		if (Chatroom.createRoom(name) != null) {
			return redirect(controllers.routes.Chat.showRoom(name));

		} else {
			return forbidden("This room name already exists, choose another.");
		}

	}

	/**
	 * Show the room page
	 * @param name
	 * @return
	 */
	public static Result showRoom(String name) {

		Chatroom room = Chatroom.activeRooms.get(name);
		if (room == null) {
			return badRequest("This chatroom does not exist.");
		}

		String wsUrl = "ws://" + Http.Context.current().request().host() + String.format("/chat/websocket/room/%s/", name);
		String username = session("username");
		//return ok(views.html.chat.room.render(wsUrl, username));
		return ok(views.html.chat.room_ko.render(wsUrl, username));

	}

    /**
     * Post a message to a room from a web GET request.
     */
    public static Result postToRoom(String roomname, String username, String message) {

        if (!Chatroom.activeRooms.containsKey(roomname)) {
            Chatroom.createRoom(roomname);
        }

        Chatroom room = Chatroom.activeRooms.get(roomname);
        User user = User.createOrGetExisting(username);
        user.avatarUrl = Avatar.AGENT;

        // For a web request, add a user without websocket connection
        UserSession session = room.getUserSession(user);
        if (session == null) {
            session = room.addUser(user, null);
        }

        Message msg = new Message(message, user, room);
        room.sendMessageToAllUsers(msg);

        return ok("message delivered to chatroom.");

    }


	/**
	 * Create a websocket
	 * @param roomName
	 * @return
	 */
	public static WebSocket<JsonNode> websocket(String roomName, String userName) {

		final Chatroom room = Chatroom.activeRooms.get(roomName);		
		final User user = User.createOrGetExisting(userName);
		session("username", userName);

		return new WebSocket<JsonNode>() {

			@Override
			public void onReady(In<JsonNode> in, final Out<JsonNode> out) {				

				// Add the user to the room
				final UserSession session = room.addUser(user, out);

				// Send the welcome message to the new user
				Message welcomeMsg = room.createSystemMessage("Welcome " + user.name + "! Currently " + (room.getUserCount() - 1) + " other folks are listening to you.");
				room.sendMessageToUser(welcomeMsg, user);
				MyLogger.info("User " + user.name + " connected to: " + room.name);

				// Send a connect msg to all users
				room.sendMessageToAllUsers(room.createSystemMessage(user.name + " entered the room."));
				
				// For each event received on the socket
				in.onMessage(new Callback<JsonNode>() {
					public void invoke(JsonNode msg) {						
						room.onIncomingMessage(msg);
					} 
				});

				// When the connection is closed
				in.onClose(new Callback0() {

					@Override
					public void invoke() {
						
						// Remove the user from the room
						room.removeUser(user);
						
						// Send a connect msg to all users
						room.sendMessageToAllUsers(room.createSystemMessage(user.name + " left the room."));
						MyLogger.info("Client disconnected (current clients in room '" + room.name + "': " + room.getUserCount() + ")");			

					}

				});

			}		
		};
	}

}
