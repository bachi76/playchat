package models;

/**
 * The available user status. They also define roles/rights.
 * @author bachi
 *
 */
public enum UserStatus {
	admin,
	outlaw,
	drunken,
	gagged,
	invisible,
	normal
}
