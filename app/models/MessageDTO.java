package models;

public class MessageDTO extends DTO {
	
	public String command = "msg";
	
	public String user;
	public String message;
	public Long timestamp;
		
}
