package models;

import play.Logger;

/**
 * Push logging data into its own chat room.
 * TODO: Temporary hack, unfinished.
 * 
 * @author bachi
 *
 */
public class MyLogger extends Logger {
	
	public static void info(String msg) {
		Logger.info (msg);
		
		try {
			Chatroom system = Chatroom.activeRooms.get("System");
			Message message = system.createSystemMessage("[Info] " + msg);
			system.sendMessageToAllUsers(message);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void warn(String msg) {
		Logger.warn (msg);
		
		try {
			Chatroom system = Chatroom.activeRooms.get("System");
			Message message = system.createSystemMessage("[WARN] " + msg);
			system.sendMessageToAllUsers(message);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void error(String msg) {
		Logger.error (msg);
		
		try {
			Chatroom system = Chatroom.activeRooms.get("System");
			Message message = system.createSystemMessage("[ERROR] " + msg);
			system.sendMessageToAllUsers(message);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
