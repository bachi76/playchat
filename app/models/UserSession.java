package models;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.WebSocket.Out;

import java.util.Calendar;
import java.util.Date;

/**
 * A user session defines the session of a specific user in a specific room.
 * @author bachi
 *
 */
public class UserSession {
	public User user;
	public Chatroom room;
	public Date joined;
	public UserStatus userStatus = UserStatus.normal;
	
	public play.mvc.WebSocket.Out<JsonNode> websocket;
	
	public UserSession(User user, Chatroom room, Out<JsonNode> websocket) {
		this.user = user;
		this.room = room;
		this.websocket = websocket;
		this.joined = Calendar.getInstance().getTime();
	}
	
	public void close() {
		room.sessions.remove(this);
		user.sessions.remove(this);
	}
	
	public UserDTO getDTO() {
		UserDTO dto = new UserDTO();
		dto.name = user.name;
		dto.status = userStatus;
        dto.avatarUrl = user.avatarUrl;
		
		return dto;
	}

	/**
	 * Send a text message to this user and room
	 * @param message
	 * @param from
	 */
	public void sendMessage(String message, User from) {
		room.sendMessageToUser(new Message(message, from, room), user);		
	}
	
	/**
	 * Send a system message to this user and room
	 * @param message
	 */
	public void sendSystemMessage(String message) {
		sendMessage(message, User.getSytemUser());
	}
	
}
