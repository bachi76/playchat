package models;

public class UnknownUserException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnknownUserException(String message) {
        super(message);
    }

    public UnknownUserException(String message, Throwable throwable) {
        super(message, throwable);
    }

}