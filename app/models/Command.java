package models;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Holds all chat commands, like '/kick somebaduser' or '/status admin somegooduser'
 */
public class Command {
	
	
	public static boolean dispatch(Message message) {
		
		String input = message.message;
		UserSession issuer = message.getUserSession();
		
		if (!input.substring(0, 1).equals("/")) {
			// No /, just do a 'say'
			return false;
		}
		
		// Cut the '/'
		input = input.substring(1);

        // TODO: Allow /tell userX you do stink (currently params with spaces do not work)
        // TODO: Get the command first, then lookup the number of arguments of the target method, then split accordingly
        // Use Command.class.getMethods()[0].getParameterCount() and cache to hashmap
        //String command = input.split(" ", 2)[0];

		List<String> tokens = new ArrayList<String>(Arrays.asList(input.split(" ")));
		String command = tokens.get(0);
		tokens.remove(0);
		Class[] cArg = new Class[tokens.size()+1];
		Object[] args = new Object[tokens.size()+1];
		args[0] = issuer;
		cArg[0] = UserSession.class;
		for (int i = 1; i < tokens.size()+1; i++) {
			cArg[i] = String.class;
			args[i] = tokens.get(i-1);
		}

        MyLogger.info("User " + (String) message.author.name + " issued '/" + input + "' in room " + message.room.name);
		Method method;
		try {
			method = Command.class.getMethod(command, cArg);
			method.invoke(null, args);
            // This line crashes play!
            // MyLogger.info("User " + issuer.user.name + " issued '/" + input + "' in room " + issuer.room.name);

            return true;

		} catch (Exception e) {
            MyLogger.info("User " + issuer.user.name + " issued illegal command '/" + input + "' in room " + issuer.room.name);

            e.printStackTrace();
			return true; // it went wrong, but still the user tried to issue  a '/' command.
		}
		
		
	}


    public static boolean quit(UserSession issuer) {
        return issuer.room.removeUser(issuer.user);
    }

    public static boolean kick(UserSession issuer, String userName) {
        if (issuer.userStatus != UserStatus.admin) {
            issuer.sendSystemMessage("Only admins can kick other users!");
            return false;
        }
        User user = User.getByName(userName);
        if (user == null) {
            issuer.sendMessage("No user '" + userName + "' found.", issuer.user);
            return false;
        }

        UserSession userSession = issuer.room.getUserSession(user);
        if (userSession != null) {
            userSession.sendSystemMessage("You were kicked by " + issuer.user.name);
            issuer.room.removeUser(user);
            Message msg = issuer.room.createSystemMessage(userName + " was kicked by " + issuer.user.name);
            issuer.room.sendMessageToAllUsers(msg);
            return true;

        } else {
            issuer.sendMessage("User '" + userName + "' is not in this room.", issuer.user);
            return false;
        }


    }
	
	public static boolean tell(UserSession issuer, String username, String msgtext) {
		
		User recipient = User.getByName(username);
		if (recipient == null) return false;
        String fromTxt = "Private message:\n";
        String toTxt = "You privately told " + recipient.name + ":\n";
        Message msg = new Message(fromTxt + msgtext, issuer.user, issuer.room);
        Message msgEcho = new Message(toTxt + msgtext, issuer.user, issuer.room);
		return issuer.room.sendMessageToUser(msg, recipient)
                && issuer.room.sendMessageToUser(msgEcho, issuer.user);
	}
	
	public static boolean status(UserSession issuer, String user, String statusString) {
		
		UserStatus status;
		if (issuer.userStatus != UserStatus.admin) {
			issuer.sendSystemMessage("Only admins can modify user status!");
			return false;
		}
		
		UserSession target = issuer.room.getUserSession(User.getByName(user));
		if (target == null) {
			issuer.sendSystemMessage("No such user: " + user);
			return false;
		}
		
		try {
			status = UserStatus.valueOf(statusString);
		} catch (Exception e) {			
			issuer.sendSystemMessage("No valid status '" + statusString + "'!");
			return false;
		}
		
		target.userStatus = status;
		issuer.room.updateUserList();
		
		Message successMsg = new Message("Admin " + issuer.user.name + " has set the status of user " + user + " to '" + statusString + "'", User.getSytemUser(), issuer.room);
		issuer.room.sendMessageToAllUsers(successMsg);
		
		return true;
	}
	
	public static boolean help(UserSession issuer) {
		return help(issuer, "");
	}
	
	public static boolean help(UserSession issuer, String command) {
		String info = "";
		
		switch (command) {

            case "tell":
                info = "Tell a person something privately. Syntax: '/tell username your private message'";

            case "kick":
                info = "As admin, you can kick other users out of the current chat room. Syntax: '/kick username'";
                break;

            case "status":
                info = "As admin, you can set any non-admin user's status. Available are: 'admin', 'outlaw', 'drunken', 'invisible', 'gagged', 'normal'. Syntax: '/status username selectedstatus'";
                break;

            case "quit":
                info = "Leave this room.";
                break;

            default:
                info = "Available commands: tell, kick, status, quit, help.\nBCS, the Bachi Chat System. Version 0.8b. Enjoy!\nBuilt using the Play! Framework.";
                break;
        }

		Message msg = new Message(info, User.getSytemUser(), issuer.room);
		return issuer.room.sendMessageToUser(msg, issuer.user);

	}

	
	private static String implode(String separator, String... data) {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < data.length - 1; i++) {
	    //data.length - 1 => to not add separator at the end
	        if (!data[i].matches(" *")) {//empty string are ""; " "; "  "; and so on
	            sb.append(data[i]);
	            sb.append(separator);
	        }
	    }
	    sb.append(data[data.length - 1]);
	    return sb.toString();
	}
	
	
}
