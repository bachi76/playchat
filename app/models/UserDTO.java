package models;

public class UserDTO extends DTO {
	
	public String name;
	public String avatarUrl;
	public UserStatus status;
}
