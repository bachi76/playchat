package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class User {

	private static HashMap<String, User> users = new HashMap<>();
	private static User systemUser = createSystemUser();
	public String name;
	public String avatarUrl;
	public Date lastLogin;
	public Date lastLogoff;
	public boolean isOnline;
	public List<UserSession> sessions = new ArrayList<>();

	private User() {}

	public static User create(String name) throws UserAlreadyExistsException {
		if (users.containsKey(name)) {
			throw new UserAlreadyExistsException("User " + name + " already exists!");
		}

		User user = new User();
		user.name = name;
		user.lastLogin = Calendar.getInstance().getTime();
        user.avatarUrl = Avatar.NORMAL;
		users.put(name, user);
		
		return user;
	}

	public static User getByName(String name) {
		return users.get(name);
	}
	
	public static User createSystemUser() {
		User sysop = createOrGetExisting("System");
		sysop.isOnline = true;
		sysop.avatarUrl = Avatar.SYSOP;

        Chatroom system = Chatroom.activeRooms.get("System");
        system.addUser(sysop, null);
        
		return sysop;
	}

	public static User createOrGetExisting(String name) {
		if (users.containsKey(name)) {
			return users.get(name);
		} else {
			try {
				return User.create(name);
			} catch (UserAlreadyExistsException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public static User getSytemUser() {
		return systemUser;
	}

}
