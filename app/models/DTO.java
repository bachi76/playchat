package models;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.Json;

/**
 * Base class for all data transfer object classes.
 *
 */
public class DTO {
	
	/**
	 * Get a JsonNode representation of this object.
	 * @return JsonNode
	 */
	public JsonNode toJson() {
		return Json.toJson(this);
	}

}
