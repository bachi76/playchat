package models;

/**
 * Created by bachi on 10.05.14.
 */
public class Avatar {

    public static String ADMIN = "jedi.jpg";
    public static String ADMIN_F = "girl2.jpg";
    public static String NORMAL = "default.png";
    public static String NORMAL_F = "girl.jpg";
    public static String AGENT = "agent1.jpg";
    public static String SYSOP = "jedijoda.jpg";
}
