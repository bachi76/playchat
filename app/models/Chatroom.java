package models;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Chatroom {

	public static HashMap<String, Chatroom> activeRooms = new HashMap<>();
	
	public String name;
	public boolean isPermanent;
	
	public List<UserSession> sessions = new ArrayList<>();

	public List<Message> messageLog = new ArrayList<>();


	public Chatroom(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Chatroom '" + name + "' - clients: " + sessions.size() + " - msgs: " + messageLog.size();
	}

	public void onIncomingMessage(JsonNode json) {

		// TODO: Handle different command types from the client
		
		Message msg = null;
		try {
			msg = Message.fromJson(json, this);

		} catch (UnknownUserException e) {
			e.printStackTrace();
		}
		
		// Pass the incoming msg to the Command dispatcher
		if (Command.dispatch(msg)) {
			MyLogger.info("Command executed: " + msg.message);
			
		} else {
			// No command - send normal chat room message
			
			// Mutate gagged user's texts :)
			if (msg.getUserSession().userStatus == UserStatus.gagged) {
				msg.message = "Nggg ghh mhhpfff!";
			}
			
			
			sendToAllUsers(msg.getDTO().toJson());				
			messageLog.add(msg);
			MyLogger.info("Msg to " + (sessions.size()) + " clients: '" + msg + "'");
		}

		

	}

	/**
	 * Send a json object to all currently connected users
	 * @param json
	 */
	public void sendToAllUsers(JsonNode json) {
		for (UserSession session : sessions) {
			if (session.websocket!=null) {
                session.websocket.write(json);
            }
		}
	}

	/**
	 * Send a chat message to all currently connected users
	 * @param msg
	 */
	public void sendMessageToAllUsers(Message msg) {
		sendToAllUsers(msg.getDTO().toJson());
	}

	/**
	 * Send a chat message to a specific user
	 * @param user
	 * @return true if message could be delivered
	 */
	public boolean sendToUser(JsonNode json, User user) {
		for (UserSession session : sessions) {
			if (session.user == user && session.websocket != null) {
				session.websocket.write(json);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Send a json object to a specific user
	 * @param msg
	 * @param user
	 * @return true if message could be delivered
	 */
	public boolean sendMessageToUser(Message msg, User user) {
		return sendToUser(msg.getDTO().toJson(), user);
	}
	
	/**
	 * Send an updated user list to all connected clients
	 */
	public void updateUserList() {
		// Send the user list
		UserListDTO userList = new UserListDTO();
		for (UserSession userSession : sessions) {
			if (userSession.userStatus != UserStatus.invisible) 
				userList.users.add(userSession.getDTO());
		}
		
		sendToAllUsers(Json.toJson(userList));
		
	}

	/**
	 * Shortcut for creating system messages.
	 * @param txt
	 * @return Message
	 */
	public Message createSystemMessage(String txt) {
		return new Message(txt, User.getSytemUser(), this);
	}
	
	/**
	 * Get the user's session in this room.
	 * @param user
	 * @return user session or null if not found
	 */
	public UserSession getUserSession(User user) {
		
		for (UserSession session : sessions) {
			if (session.user == user) return session;
		}
		
		return null;
	}
	
	
	/**
	 * The the number of users currently in this room
	 * @return
	 */
	public int getUserCount() {
		return sessions.size();
	}

	
	/**
	 * Add a new user to this chat room. Generate and return the user session.
	 * 
	 * @param user
	 * @param out websocket
	 * @return user session
	 */
	public UserSession addUser(User user, play.mvc.WebSocket.Out<JsonNode> out) {
		
		UserSession session = new UserSession(user, this, out);
		session.websocket = out;
		
		// The first user of a room gets admin status
		if (sessions.size() == 0) {
			session.userStatus = UserStatus.admin;
		}

		// Add the new session to both the chatroom and the user, and update the userlist of all clients
		sessions.add(session);
		user.sessions.add(session);
		updateUserList();
		
		return session;
	}
	
	public boolean removeUser(User user) {
		UserSession session = getUserSession(user);
		if (session == null) {
			MyLogger.warn("Can't remove user " + user.name + " from room " + name + ": User not here.");
			return false;
		}

        this.sendMessageToAllUsers(createSystemMessage("User " + user.name + " left."));
		session.close();
		updateUserList();
		
		// Close the room, if the last user left and this is no permanent room.
		if (sessions.size() == 0 && !isPermanent) {
			Chatroom.removeRoom(this);
		}
		return true;
	}

	/**
	 * Create a new chat room. 
	 * @param name
	 * @return the new chat room if the room was created, null otherwise
	 */
	public static Chatroom createRoom(@NotNull String name) {
		if (!activeRooms.containsKey(name)) {
			Chatroom room = new Chatroom(name);
			room.onCreate();
			Chatroom.activeRooms.put(name, room);
			return room;
		
		} else {
			MyLogger.warn("Could not create room '" + name + "' - this room already exists.");
			return null;
		}
	}
	
	public static boolean removeRoom(@NotNull Chatroom room) {
		if (activeRooms.containsKey(room.name)) {
			room.onClose();
			activeRooms.remove(room.name);
			return true;
			
		} else {
			MyLogger.error("Cannot remove non-active room " + room.name);
			return false;
		}
	}

	/**
	 * This room is being closed.
	 */
	private void onClose() {
		MyLogger.info("Room '" + name + "' closed.");
	}
	
	/**
	 * This room was just created
	 */
	private void onCreate() {
		if (!name.equals("System")) {
            MyLogger.info("Room '" + name + "' created.");
        }
	}
}
