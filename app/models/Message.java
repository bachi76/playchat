package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


/**
 * A chat message.
 * 
 * @author bachi
 *
 */
public class Message {
	
	public User author;
	public Chatroom room;
	public Date timestamp;
	public String message;
	
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public Message(String message, User author, Chatroom room) {
		this.message = message;
		this.author = author;
		this.room = room;
		
		timestamp = Calendar.getInstance().getTime();
	}
	
	public static Message fromJson(JsonNode json, Chatroom room) throws UnknownUserException {	
		
		try {
			MessageDTO dto = objectMapper.readValue(json.toString(), MessageDTO.class);
			
			User author = User.getByName(dto.user);
			if (author == null) {
				throw new UnknownUserException("User " + dto.user + " is not known.");
			}
			Message msg = new Message(dto.message, author, room);
			return msg;
			
		} catch (IOException e) {
			System.out.println("ERROR: Could not parse incoming message: " + json.toString());
			e.printStackTrace();
			return null;
		}

	}

	public MessageDTO getDTO() {
		
		MessageDTO dto = new MessageDTO();
		dto.user = author.name;
		dto.message = message;
		dto.timestamp = timestamp.getTime();
		
		return dto;	
	}
	
	
	/**
	 * Get the user session this message was sent on, if it still exists.
	 * @return user session or null if it is no longer available in this room
	 */
	public UserSession getUserSession() {
		return room.getUserSession(author);
	}
}
