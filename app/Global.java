
import models.Chatroom;
import models.MyLogger;
import play.Application;
import play.GlobalSettings;


public class Global extends GlobalSettings {

	@Override
	public void onStart(Application app) {

        // Create the system chat room
        Chatroom system = Chatroom.createRoom("System");
        system.isPermanent = true;

		// Create the default chat room
		Chatroom lobby = Chatroom.createRoom("Lobby");
		lobby.isPermanent = true;
		
		MyLogger.info("Application has started");

	}  

	@Override
	public void onStop(Application app) {
		MyLogger.info("Application shutdown...");
	}  

}