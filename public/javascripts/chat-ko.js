/**
 * All the chat client code.
 *
 */
while (username == "") {
    username = prompt("Please enter your nick name", "Harry Potter");
}

wsUri = wsUri + "?userName=" + username;

var viewModel;
var output;

function init() {
    output = document.getElementById("chat-messages-inner");

    viewModel = new ChatroomViewModel();
    ko.applyBindings(viewModel);
    setupWebSocket();

}

function setupWebSocket() {
    websocket = new WebSocket(wsUri);
    websocket.onopen = function(evt) {
        onOpen(evt)
    };
    websocket.onclose = function(evt) {
        onClose(evt)
    };
    websocket.onmessage = function(evt) {
        onMessage(evt)
    };
    websocket.onerror = function(evt) {
        onError(evt)
    };
}

function onOpen(evt) {
    //writeToScreen(' <p class="online" id="msg-3" style="display: block;"><span>Connected.</span></p>');
}

function onClose(evt) {
    writeToScreen(' <p class="offline" id="msg-3" style="display: block;"><span>User Neytiri left the chat</span></p>');
}

function onMessage(evt) {
    var data = $.parseJSON(evt.data)
    switch(data.command) {

        case "msg":
            handleChatMessage(data)
            showFlashMessage(data.message);
            break;

        case "userlist":
            handleUserListUpdate(data);
            break;

        default:
            console.log("No such command type: " + evt.command);
            break;
    }

}

function showFlashMessage(message, type) {
    $("#alert-message").html(message);
    $(".alert").fadeIn(100);
    setTimeout('$(".alert").fadeOut(500);', 5000);

}

function handleChatMessage(msg) {

    var message = new Message(msg);
    viewModel.messages.push(message);

    $('.chat-messages').animate({
        scrollTop : $('.chat-messages').prop('scrollHeight')
    });

}

function handleUserListUpdate(msg) {

    var mappedUsers = $.map(msg.users, function(item) {
        return new User(item)
    });

    viewModel.users(mappedUsers);
}



function handleLoginRequest(evt) {

}

function onError(evt) {
    writeToScreen(' <p class="offline" id="msg-3" style="display: block;"><span style="color: red;">ERROR:</span> ' + evt.data + '</p>');
}

function doSend(message) {
    var json = '{"user":"' + username + '","message":"' + message + '"}';
    websocket.send(json);
}

function writeToScreen(html) {
    $('.chat-messages-inner').append(html);
}

function sendButtonClick() {
    doSend($("#msg-box").val());
    $("#msg-box").val('');
}


window.addEventListener("load", init, false);

$("#msg-box").keypress(function(event) {
    if (event.which == 13)
        sendButtonClick();
});


/** Knockout View model **/

function User(data) {
    //this.name = ko.observable(data.name);
    //this.status = ko.observable(data.status);
    //this.avatarUrl = ko.observable(data.avatarUrl);
    this.name = data.name;
    this.status = data.status;
    this.avatarUrl = data.avatarUrl;
}

function Message(data) {
    var date = new Date(data.timestamp);

    this.user = data.user;
    this.timestamp = data.timestamp;
    this.time = date.toLocaleTimeString();
    this.message = data.message;
}

function Alert(data) {
    this.message = data.message;
    data.type == null ? "" : this.type = data.type;
}

// Overall viewmodel for this screen, along with initial state
function ChatroomViewModel() {
    var self = this;

    /** Data */

    // FIXME: These arrays always stay empty!

        // User list
    self.users = ko.observableArray([]);

    // Chat messages
    self.messages = ko.observableArray([]);

    // Flash alert messages
    self.alerts = ko.observableArray([]);


    /** Operations **/

    // Get an avatar for a given username
    self.getAvatarUrl = function (avatarUrl) {
        url = avatarBaseUrl + (avatarUrl ==  null ? "default.png" : avatarUrl);
        console.log("Avatar url: " + url);
        return url;
    }

    self.getAvatarUrlForUserName = function (userName) {
        for (i=0; i<self.users().length; i++) {
            var user = self.users()[i];
            if (user.name == userName) return self.getAvatarUrl(user.avatarUrl);
        }
        return self.getAvatarUrl(null);
    }

}




