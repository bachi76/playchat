/**
 * All the chat client code.
 * 
 */	
	while (username == "") {
		username = prompt("Please enter your nick name", "Harry Potter");
	}

	wsUri = wsUri + "?userName=" + username;

	var output;
	var userList;

	function init() {
		output = document.getElementById("chat-messages-inner");
		setupWebSocket();
	}

	function setupWebSocket() {
		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) {
			onOpen(evt)
		};
		websocket.onclose = function(evt) {
			onClose(evt)
		};
		websocket.onmessage = function(evt) {
			onMessage(evt)
		};
		websocket.onerror = function(evt) {
			onError(evt)
		};
	}

	function onOpen(evt) {
		writeToScreen(' <p class="online" id="msg-3" style="display: block;"><span>Connected.</span></p>');
	}

	function onClose(evt) {
		writeToScreen(' <p class="offline" id="msg-3" style="display: block;"><span>User Neytiri left the chat</span></p>');
	}

	function onMessage(evt) {
		var data = $.parseJSON(evt.data)
		switch(data.command) {
			
			case "msg":
				handleChatMessage(data)
				break;
			
			case "userlist":
				handleUserListUpdate(data);
				break;
			
			default:
				console.log("No such command type: " + evt.command);
				break;						
		}
		
	}
	
	function handleChatMessage(msg) {		
		var date = new Date(msg.timestamp);

		html = ' <p id="msg-1" class="user-neytiri" style="display: block;">' + ' 	<img src="' + getAvatarUrl(msg.user) + '" alt="">' + ' 	<span class="msg-block">' + '		<strong>' + msg.user + '</strong> <span class="time">' + date.toLocaleTimeString() + '</span>' + '		<span class="msg">' + msg.message + '</span>' + '	</span>';

		writeToScreen(html);
		$('.chat-messages').animate({
			scrollTop : $('.chat-messages').prop('scrollHeight')
		});
	}
	
	function handleUserListUpdate(msg) {
		var html = '';
		
		$.each(msg.users, function(k, v) {
			var stat = "[" + v.status + "]";
		 	html += '<li id="user-michelle" class="online"><a href="#"><img alt="" src="' + getAvatarUrl(v.name) + '"> <span>' + v.name + " " + stat + '</span></a></li>';
		});
		
		$('.contact-list').html(html);
		
		// Store the user list of this room
		userList = msg.users;
		
	}
	
	function getAvatarUrl(username) {
		
		if (userList != null) {
			$.each(userList, function(k, user) {
				if (user.name == username) {
					return avatarBaseUrl + (user.avatarUrl ==  null ? "default.png" : user.avatarUrl);
				}	
			});
		}
		
		return avatarBaseUrl + "default.png";
	}
	
	function handleLoginRequest(evt) {
		
	}
	
	function onError(evt) {
		writeToScreen(' <p class="offline" id="msg-3" style="display: block;"><span style="color: red;">ERROR:</span> ' + evt.data + '</p>');
	}

	function doSend(message) {
		var json = '{"user":"' + username + '","message":"' + message + '"}';
		websocket.send(json);
	}

	function writeToScreen(html) {
		$('.chat-messages-inner').append(html);
	}

	function sendButtonClick() {
		doSend($("#msg-box").val());
		$("#msg-box").val('');
	}


	window.addEventListener("load", init, false);

	$("#msg-box").keypress(function(event) {
		if (event.which == 13)
			sendButtonClick();
	});
